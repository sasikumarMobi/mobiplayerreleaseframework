// Copyright 2018 Google Inc. All rights reserved.

#import "CdmWrapper.h"
@protocol LicenseManagerDataSource <NSObject>
-(void)fetchLicenseWithData:(NSData *)data
                  contentId:(NSString *)contentId
            completionBlock:(void (^)(NSData *, NSError *))completionBlock;
@end
@interface LicenseManager : NSObject <iOSCdmDelegate>
+ (void)startup;
+ (LicenseManager *)sharedInstance;
@property (nonatomic, weak) id <LicenseManagerDataSource> datasource;
@property (nonatomic, strong) NSString * contentId;
@end
