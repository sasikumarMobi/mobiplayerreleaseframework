//
//  WidevineHelper.h
//  MOBIPlayer
//
//  Created by Sasikumar on 06/06/19.
//  Copyright © 2019 Sasikumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVKit/AVKit.h>
#import "LicenseManager.h"
#import "SideLoadSubtitle.h"
#import "MediaStream.h"
NS_ASSUME_NONNULL_BEGIN

@interface WidevineHelper : NSObject
- (void)processMPD:(NSURL *)MPDURL
         isOffline:(BOOL)isOffline
         contentId:(NSString *) contentId
licenseManagerDataSource:(id <LicenseManagerDataSource>)datasource
    withCompletion:(void (^)(AVPlayerItem * _Nullable, NSMutableArray<SideLoadSubtitle *> * _Nullable, NSMutableArray<MediaStream *> * _Nullable, NSError * _Nullable))completion;
- (void)stopServer;
- (void)removeObservers;
- (void)addObservers;
@end

NS_ASSUME_NONNULL_END
