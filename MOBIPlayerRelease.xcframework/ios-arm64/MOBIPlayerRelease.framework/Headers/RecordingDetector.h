// Copyright 2016 Google Inc. All rights reserved.

#import <Foundation/Foundation.h>

extern NSString *kRecordingStatusChangedNotification;

@interface RecordingDetector : NSObject

+ (instancetype)sharedDetector;
+ (void)startChecking;
+ (void)stopChecking;
- (BOOL)isRecording;

@end
